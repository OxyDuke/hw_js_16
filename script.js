//виконано за допомогою динамічного програмування знизу вгору.
function generalizedFibonacci(F0, F1, n) {
  if (n === 0) {
    return F0;
  } else if (n === 1) {
    return F1;
  }

  const fibArray = new Array(Math.abs(n) + 1);

  fibArray[0] = F0;
  fibArray[1] = F1;

  for (let i = 2; i <= Math.abs(n); i++) {
    fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
  }

  if (n < 0 && n % 2 === 0) {
    fibArray[Math.abs(n)] = -fibArray[Math.abs(n)];
  }

  return fibArray[Math.abs(n)];
}

const n = parseInt(prompt("Введіть порядковий номер n:"));

const F0 = 0;
const F1 = 1;

const result = generalizedFibonacci(F0, F1, n);
alert(`n-е обобщене число Фібоначчі дорівнює: ${result}`);
